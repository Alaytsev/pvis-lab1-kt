import com.google.gson.GsonBuilder
import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import javax.imageio.ImageIO
import kotlin.math.absoluteValue

/**
 * Created by ada on 17-Jan-18.
 */

const val NORMALIZED_ARRAY_LENGTH = 64
const val THREAD_COUNT = 2

val references = HashMap<String, RGBBarChart>()

fun createBarChart(image: BufferedImage): RGBBarChart {
    val red = FloatArray(256, { 0f })
    val green = FloatArray(256, { 0f })
    val blue = FloatArray(256, { 0f })
    for (i in 0 until image.width) {
        for (j in 0 until image.height) {
            val color = Color(image.getRGB(i, j))
            red[color.red]++
            green[color.green]++
            blue[color.blue]++
        }
    }
    return RGBBarChart(red, green, blue)
}

fun createNormalizedBarChart(image: BufferedImage): RGBBarChart {
    val rgbBarChart = createBarChart(image)
    var acc = 0
    var accRed = 0f
    var accGreen = 0f
    var accBlue = 0f
    var outIndex = 0

    val shortRed = FloatArray(NORMALIZED_ARRAY_LENGTH, { 0f })
    val shortGreen = FloatArray(NORMALIZED_ARRAY_LENGTH, { 0f })
    val shortBlue = FloatArray(NORMALIZED_ARRAY_LENGTH, { 0f })
    val pixelsCount = image.width * image.height

    rgbBarChart.red.forEachIndexed { i, _ ->
        accRed += rgbBarChart.red[i]
        accGreen += rgbBarChart.green[i]
        accBlue += rgbBarChart.blue[i]

        if (acc >= (rgbBarChart.red.size.toFloat() / NORMALIZED_ARRAY_LENGTH) || i == rgbBarChart.red.lastIndex) {

            shortRed[outIndex] = (accRed / pixelsCount)
            shortGreen[outIndex] = (accGreen / pixelsCount)
            shortBlue[outIndex] = (accBlue / pixelsCount)

            acc = 0
            accRed = 0f
            accGreen = 0f
            accBlue = 0f
            outIndex++
        }
        acc++
    }
    return RGBBarChart(shortRed, shortGreen, shortBlue)
}

fun learn(season: String) {
    val barCharts = ArrayList<RGBBarChart>()
    File("ref")
            .walkTopDown()
            .filter { file -> file.isDirectory }
            .filter { file -> file.name.contains(season) || file.name.contains(season.toLowerCase()) }
            .flatMap { file -> file.walkTopDown() }
            .filterNot { file -> file.parentFile.name.contains("pass") }
            .toMutableList()
            .parallelStream()
            .filter { file ->
                file.name.contains(".jpg")
                        || file.name.contains(".jpeg")
                        || file.name.contains(".png")
            }
            .forEach { file ->
                println("Loading file: ${file.absolutePath} size:${file.readBytes().size}")
                val barChart = createNormalizedBarChart(ImageIO.read(file))
                barChart.season = season
                barCharts.add(barChart)
            }

    val averageRed = FloatArray(NORMALIZED_ARRAY_LENGTH, { 0f })
    val averageGreen = FloatArray(NORMALIZED_ARRAY_LENGTH, { 0f })
    val averageBlue = FloatArray(NORMALIZED_ARRAY_LENGTH, { 0f })
    for (i in 0 until NORMALIZED_ARRAY_LENGTH) {
        barCharts.forEach { barChart ->
            averageRed[i] += barChart.red[i]
            averageGreen[i] += barChart.green[i]
            averageBlue[i] += barChart.blue[i]
        }
        averageRed[i] = (averageRed[i] / barCharts.size)
        averageGreen[i] = (averageGreen[i] / barCharts.size)
        averageBlue[i] = (averageBlue[i] / barCharts.size)
    }
    println("**********************")
    println("Checking RED average: ${averageRed.sum()}")
    println("Checking GREEN average: ${averageGreen.sum()}")
    println("Checking BLUE average: ${averageBlue.sum()}")
    println("**********************")

    if (!File("ref").exists() || !File("ref").isDirectory) {
        File("ref").mkdir()
    }
    val rgbBarChart = RGBBarChart(season, averageRed, averageGreen, averageBlue)
    references[season] = rgbBarChart
    File("ref${File.separatorChar}average_${season.toLowerCase()}.txt")
            .writeText(GsonBuilder().create().toJson(rgbBarChart))
}

fun difference(source: RGBBarChart, reference: RGBBarChart): Float {
    var diff = 0f
    if (reference.red.size != NORMALIZED_ARRAY_LENGTH) {
        throw IllegalArgumentException("Different size of reference and source")
    }
    reference.red.forEachIndexed { index, _ ->
        diff += (source.red[index] - reference.red[index]).absoluteValue +
                (source.green[index] - reference.green[index]).absoluteValue +
                (source.blue[index] - reference.blue[index]).absoluteValue
    }
    return diff
}

fun processing() {
    File("check")
            .walkTopDown()
            .filter { file -> file.isDirectory }
            .flatMap { file -> file.walkTopDown() }
            .filterNot { file -> file.parentFile != null && file.parentFile.name.contains("pass") }
            .filter { file ->
                file.name.contains(".jpg")
                        || file.name.contains(".jpeg")
                        || file.name.contains(".png")
            }
            .toMutableList()
            .parallelStream()
            .forEach { file ->
                println("Loading file: ${file.absolutePath} size:${file.readBytes().size}")
                val rgbBarChartForCurrentImage = createNormalizedBarChart(ImageIO.read(file))
                val resultMap = HashMap<String, Float>()
                resultMap["Winter"] = difference(rgbBarChartForCurrentImage, references["Winter"]!!)
                resultMap["Spring"] = difference(rgbBarChartForCurrentImage, references["Spring"]!!)
                resultMap["Summer"] = difference(rgbBarChartForCurrentImage, references["Summer"]!!)
                resultMap["Autumn"] = difference(rgbBarChartForCurrentImage, references["Autumn"]!!)
                val resultSeason = resultMap.minBy { (_, value) -> value }?.key
                println("${file.name} is $resultSeason")
                if (!File("output").exists() || !File("output").isDirectory) {
                    File("output").mkdir()
                }
                file.copyTo(
                        File("output${File.separatorChar}${file.nameWithoutExtension}_" +
                                "$resultSeason.${file.extension}"), true)
            }
}

fun loadReferences(): Int {
    println("Loading references...")
    File("ref")
            .walkTopDown()
            .toMutableList()
            .parallelStream()
            .filter { file -> file.isFile && file.name.contains(".txt") }
            .forEach { file ->
                println("Find reference: ${file.absolutePath}")
                val ref = GsonBuilder().create().fromJson<RGBBarChart>(file.readText(), RGBBarChart::class.java)
                if (ref.season.isNotEmpty()) {
                    references[ref.season] = ref
                } else throw IllegalArgumentException("Reference must contains a season's name!")
            }
    println("Loaded references: ${references.size}")
    return references.size
}

fun startLearning() {
    val jobs = arrayListOf<Callable<Any>>(
            Callable { learn("Winter") },
            Callable { learn("Spring") },
            Callable { learn("Summer") },
            Callable { learn("Autumn") })
    Executors.newFixedThreadPool(THREAD_COUNT).invokeAll(jobs)
}


fun main(args: Array<String>) {
    val startTime = LocalDateTime.now()
    startLearning()
    loadReferences()
    processing()
    println("Elapsed time: ${ChronoUnit.MILLIS.between(startTime, LocalDateTime.now())} milliseconds")
}

