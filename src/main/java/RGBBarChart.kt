import com.google.gson.GsonBuilder
import java.util.*

/**
 * Created by ada on 17-Jan-18.
 */

class RGBBarChart(val red: FloatArray, val green: FloatArray, val blue: FloatArray) {
    constructor(season: String, red: FloatArray, green: FloatArray, blue: FloatArray) : this(red, green, blue) {
        this.season = season
    }

    var season = ""

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RGBBarChart

        if (!Arrays.equals(red, other.red)) return false
        if (!Arrays.equals(green, other.green)) return false
        if (!Arrays.equals(blue, other.blue)) return false
        if (!season.contentEquals(other.season)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = Arrays.hashCode(red)
        result = 31 * result + Arrays.hashCode(green)
        result = 31 * result + Arrays.hashCode(blue)
        result = 31 * result + season.hashCode()
        return result
    }

    override fun toString(): String {
        return GsonBuilder().create().toJson(this)
    }
}